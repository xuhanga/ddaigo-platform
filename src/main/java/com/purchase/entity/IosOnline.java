package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "IosOnline")
public class IosOnline {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	private Integer isOnline;
	private String version;
}
