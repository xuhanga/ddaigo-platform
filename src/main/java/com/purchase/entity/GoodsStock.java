package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_goods_stock")
public class GoodsStock {
	@Id
	@GeneratedValue(generator = "JDBC")
    private Integer id;

    private Integer goodsId;
    
    private String specsName;
    
    private Integer stockNum;

}