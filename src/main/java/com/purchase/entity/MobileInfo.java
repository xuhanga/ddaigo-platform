package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="p_mobile_info")
public class MobileInfo {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	private String wxUid;
	private String os;
	private String imei;
	private String mac;
	private String phoneBrand;
	private String phoneModel;
	private String operator;
}
