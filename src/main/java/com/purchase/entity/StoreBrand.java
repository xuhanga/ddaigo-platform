package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_store_brand")
public class StoreBrand {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;
	
	private Integer brandId;

	private String storeNo;

}