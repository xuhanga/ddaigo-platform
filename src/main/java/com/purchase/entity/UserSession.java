package com.purchase.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="user_session")
public class UserSession {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	
    private String wxUid;

    private String sessionId;

    private String openId;

    private String sessionKey;

    private Long createTime;

}