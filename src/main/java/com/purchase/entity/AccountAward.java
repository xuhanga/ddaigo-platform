package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "b_account_award")
public class AccountAward {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer awardId;

    private String parentWxUid;

    private String wxUid;

    private String inviteCode;

    private String orderId;

    private String incomeGrade;

    private BigDecimal incomeAmount;

    private Date incomeDate;

    private Date createTime;

    public Integer getAwardId() {
        return awardId;
    }

    public void setAwardId(Integer awardId) {
        this.awardId = awardId;
    }

    public String getParentWxUid() {
        return parentWxUid;
    }

    public void setParentWxUid(String parentWxUid) {
        this.parentWxUid = parentWxUid;
    }

    public String getWxUid() {
        return wxUid;
    }

    public void setWxUid(String wxUid) {
        this.wxUid = wxUid;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIncomeGrade() {
        return incomeGrade;
    }

    public void setIncomeGrade(String incomeGrade) {
        this.incomeGrade = incomeGrade;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Date getIncomeDate() {
        return incomeDate;
    }

    public void setIncomeDate(Date incomeDate) {
        this.incomeDate = incomeDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}