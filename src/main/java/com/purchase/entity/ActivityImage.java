package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "p_activity_image")
public class ActivityImage {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private Integer mediaType;
    
    private Integer activityId;
    
    private String mediaUrl;

    private Date createTime;

  
}
