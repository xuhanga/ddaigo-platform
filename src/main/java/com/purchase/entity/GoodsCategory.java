package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

//地区码表
@Data
@Table(name = "p_goods_category")
public class GoodsCategory {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer categoryId;

	private Integer categoryPid;

	private String categoryName;

	private Integer categorySort;

	private Integer categoryStatus;

	private Date createTime;



}
