package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "b_account_income")
public class AccountIncome {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer accountId;

    private String masterUid;

    private String parentWxUid;

    private String wxUid;

    private String inviteCode;

    private String userStatus;

    private String personName;

    private String mobile;

    private String weixinId;

    private String alipayId;

    private Integer totalMember;

    private Integer totalVendor;

    private BigDecimal totalVendorJoin;

    private BigDecimal totalMemberJoin;

    private Integer totalOrder;

    private BigDecimal totalBonusAmount;

    private BigDecimal totalAwardAmount;

    private BigDecimal preIncomeAmount;

    private BigDecimal totalAmount;

    private BigDecimal canCashoutAmount;

    private BigDecimal cashoutAmount;

    private Date createTime;

    private Date accountTime;

    private Date lastCashoutTime;

    private BigDecimal totalSubvendorAmount;

    private BigDecimal totalTeamAmount;

    private BigDecimal totalDirectorAmount;

    private BigDecimal totalMasterAmount;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getMasterUid() {
        return masterUid;
    }

    public void setMasterUid(String masterUid) {
        this.masterUid = masterUid;
    }

    public String getParentWxUid() {
        return parentWxUid;
    }

    public void setParentWxUid(String parentWxUid) {
        this.parentWxUid = parentWxUid;
    }

    public String getWxUid() {
        return wxUid;
    }

    public void setWxUid(String wxUid) {
        this.wxUid = wxUid;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWeixinId() {
        return weixinId;
    }

    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId;
    }

    public String getAlipayId() {
        return alipayId;
    }

    public void setAlipayId(String alipayId) {
        this.alipayId = alipayId;
    }

    public Integer getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(Integer totalMember) {
        this.totalMember = totalMember;
    }

    public Integer getTotalVendor() {
        return totalVendor;
    }

    public void setTotalVendor(Integer totalVendor) {
        this.totalVendor = totalVendor;
    }

    public BigDecimal getTotalVendorJoin() {
        return totalVendorJoin;
    }

    public void setTotalVendorJoin(BigDecimal totalVendorJoin) {
        this.totalVendorJoin = totalVendorJoin;
    }

    public BigDecimal getTotalMemberJoin() {
        return totalMemberJoin;
    }

    public void setTotalMemberJoin(BigDecimal totalMemberJoin) {
        this.totalMemberJoin = totalMemberJoin;
    }

    public Integer getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(Integer totalOrder) {
        this.totalOrder = totalOrder;
    }

    public BigDecimal getTotalBonusAmount() {
        return totalBonusAmount;
    }

    public void setTotalBonusAmount(BigDecimal totalBonusAmount) {
        this.totalBonusAmount = totalBonusAmount;
    }

    public BigDecimal getTotalAwardAmount() {
        return totalAwardAmount;
    }

    public void setTotalAwardAmount(BigDecimal totalAwardAmount) {
        this.totalAwardAmount = totalAwardAmount;
    }

    public BigDecimal getPreIncomeAmount() {
        return preIncomeAmount;
    }

    public void setPreIncomeAmount(BigDecimal preIncomeAmount) {
        this.preIncomeAmount = preIncomeAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getCanCashoutAmount() {
        return canCashoutAmount;
    }

    public void setCanCashoutAmount(BigDecimal canCashoutAmount) {
        this.canCashoutAmount = canCashoutAmount;
    }

    public BigDecimal getCashoutAmount() {
        return cashoutAmount;
    }

    public void setCashoutAmount(BigDecimal cashoutAmount) {
        this.cashoutAmount = cashoutAmount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getAccountTime() {
        return accountTime;
    }

    public void setAccountTime(Date accountTime) {
        this.accountTime = accountTime;
    }

    public Date getLastCashoutTime() {
        return lastCashoutTime;
    }

    public void setLastCashoutTime(Date lastCashoutTime) {
        this.lastCashoutTime = lastCashoutTime;
    }

    public BigDecimal getTotalSubvendorAmount() {
        return totalSubvendorAmount;
    }

    public void setTotalSubvendorAmount(BigDecimal totalSubvendorAmount) {
        this.totalSubvendorAmount = totalSubvendorAmount;
    }

    public BigDecimal getTotalTeamAmount() {
        return totalTeamAmount;
    }

    public void setTotalTeamAmount(BigDecimal totalTeamAmount) {
        this.totalTeamAmount = totalTeamAmount;
    }

    public BigDecimal getTotalDirectorAmount() {
        return totalDirectorAmount;
    }

    public void setTotalDirectorAmount(BigDecimal totalDirectorAmount) {
        this.totalDirectorAmount = totalDirectorAmount;
    }

    public BigDecimal getTotalMasterAmount() {
        return totalMasterAmount;
    }

    public void setTotalMasterAmount(BigDecimal totalMasterAmount) {
        this.totalMasterAmount = totalMasterAmount;
    }
}