/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: WxAppLoginController.java
 * @Package: com.ddyx.rest
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年11月29日 下午6:18:26
 * 
 * *************************************
 */
package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.IUserAccessService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;


/**
 * @ClassName: WxAppLoginController.java
 * @Module: 微信小程序用户登录请求控制器模块
 * @Description: 接收微信小程序扫码页面请求（首次授权登录请求）、微信账号登录验证请求、 、 保存已授权的用户登录信息请求、获取叮店用户角色
 * 
 * @author: liuhoujie
 * @date: 2017年11月29日 下午6:18:26
 * 
 */
@Controller
@RequestMapping("/wxmp")
public class WxAppLoginController {

	@Resource
	IUserAccessService userAccessService;

	/**
	 * 使用微信账号登录请求(OLD)
	 * @param requestForm
	 * @return
	 */
//	@ResponseBody
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public ResponseForm login(@RequestBody RequestForm requestForm) {
//		// 从request header中获取微信用户sessionId，
//		// 如果有sessionId的则调用checkSession验证会话是否过期，
//		// 否则调用登录keepSession保持会话
//		// todo
//		// request.getSession().getAttribute("wxUid");
//		return userAccessService.wechatUserlogin(requestForm);
//	}


	/**
	 * 微信授权(新版,合并授权和保存用户信息)
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/authorize", method = RequestMethod.POST)
	public ResponseForm authorize(@RequestBody RequestForm param) {
		// 从request header中获取微信用户sessionId，
		// 如果有sessionId的则调用checkSession验证会话是否过期，
		// 否则调用登录keepSession保持会话
		// todo
		// request.getSession().getAttribute("wxUid");
		return userAccessService.authorize(param);
	}

	/**
	 * 微信授权(校园活动版)
	 * 记录inviteCode(推广手机号)
	 * 返回用户绑定手机号
	 * 返回sessionId
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/authorize2", method = RequestMethod.POST)
	public ResponseForm authorize2(@RequestBody RequestForm requestForm) {
		// 从request header中获取微信用户sessionId，
		// 如果有sessionId的则调用checkSession验证会话是否过期，
		// 否则调用登录keepSession保持会话
		// todo
		// request.getSession().getAttribute("wxUid");
		return userAccessService.authorize2(requestForm);
	}

	
	/**
	 * 保存已授权的用户登录信息请求(OLD)
	 * 
	 * @param requestForm
	 * @return
	 */
//	@ResponseBody
//	@RequestMapping(value = "/saveUserInfo", method = RequestMethod.POST)
//	public ResponseForm saveUserInfo(@RequestBody RequestForm requestForm) {
//
//		return userAccessService.saveUserInfo(requestForm);
//	}

	/**
	 * 保存已授权的用户登录信息请求(NEW)
	 * 加入了手机号条件
	 * @param requestForm
	 * @return
	 */
//	@ResponseBody
//	@RequestMapping(value = "/newSaveUserInfo", method = RequestMethod.POST)
//	public ResponseForm newSaveUserInfo(@RequestBody RequestForm requestForm) {
//
//		return userAccessService.newSaveUserInfo(requestForm);
//	}
	
	/**
	 * 保存已授权的用户登录信息请求(当前小程序用的版本)
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveUserInfo2", method = RequestMethod.POST)
	public ResponseForm saveUserInfo2(@RequestBody RequestForm requestForm) {

		return userAccessService.saveUserInfo2(requestForm);
	}
	
	/**
	 * 获取叮店用户角色
	 * 
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserRole", method = RequestMethod.POST)
	public ResponseForm getUserRole(@RequestBody RequestForm requestForm) {

		return userAccessService.getUserRole(requestForm);
	}

	/**
	 * 微信用户会话状态验证
	 * 
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkSession", method = RequestMethod.POST)
	public ResponseForm checkSession(@RequestBody RequestForm requestForm) {

		return null;
	}
	
}
