package com.purchase.rest;

import com.purchase.service.WithdrawRecordService;
import com.purchase.service.WxToolService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/withdrawRecord")
public class WithdrawRecordController {

    @Autowired
    WithdrawRecordService withdrawRecordService;

    @ResponseBody
    @RequestMapping(value = "addWithdrawRecord", method = RequestMethod.POST)
    public ResponseForm addWithdrawRecord(@RequestBody RequestForm requestForm) {
        return withdrawRecordService.addWithdrawRecord(requestForm);
    }

    @ResponseBody
    @RequestMapping(value = "getWithdrawRecordList", method = RequestMethod.POST)
    public ResponseForm getWithdrawRecordList(@RequestBody RequestForm requestForm) {
        return withdrawRecordService.getWithdrawRecordList(requestForm);
    }

}
