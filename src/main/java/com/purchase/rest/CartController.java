package com.purchase.rest;

import com.purchase.service.CartService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/cart")
public class CartController {

    @Resource
    private CartService cartService;

    /**
     * 获取购物车
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public ResponseForm get(@RequestBody RequestForm param) {
        return cartService.getCart(param);
    }

    /**
     * 添加商品进购物车
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseForm add(@RequestBody RequestForm param) {
        return cartService.insertOrUpdateGoodsToCart(param);
    }

    /**
     * 修改购物车商品属性
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public ResponseForm modify(@RequestBody RequestForm param) {
        return cartService.modifyGoodsInCart(param);
    }

    /**
     * 删除购物车内商品
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public ResponseForm remove(@RequestBody RequestForm param) {
        return cartService.removeGoodsInCart(param);
    }

    /**
     * 清空购物车
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/empty", method = RequestMethod.POST)
    public ResponseForm empty(@RequestBody RequestForm param) {
        return cartService.emptyCart(param);
    }

}
