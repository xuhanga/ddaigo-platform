package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.util.RedisUtil;
import com.purchase.util.ResponseForm;


@Controller
@RequestMapping("/redis_test")
public class RedisTestController {
	
	@Resource
	RedisUtil redisUItil;
	
	@ResponseBody
	@RequestMapping(value = "/add",method = RequestMethod.GET)
	public ResponseForm add() {
		ResponseForm result = new ResponseForm();
		redisUItil.set("login_code_13269621006", "166578",120);
		return result;
	}
	

}
