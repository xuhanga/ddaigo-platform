package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.StoreActivityService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/activity")
public class StoreActivityController {
	
	@Resource
	StoreActivityService storeActivityService;
	
	
	/**
	 * 获取活动列表后台使用
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/admin/activityList", method = RequestMethod.POST)
	public ResponseForm getactivityListAdmin(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getactivityListAdmin(param,request);
	}
	/**
	 * 查所有活动的列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/admin/allActivityList", method = RequestMethod.POST)
	public ResponseForm getAllActivityListAdmin(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getAllActivityListAdmin(param,request);
	}
	
	/**
	 * 获取活动列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/activityList", method = RequestMethod.POST)
	public ResponseForm activityList(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.activityList(param,request);
	}
	
	/**
	 * 获取单个活动
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getActivityDetail", method = RequestMethod.POST)
	public ResponseForm getActivityDetail(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getActivityDetail(param,request);
	}
	
	/**
	 * 新增活动
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addActivity", method = RequestMethod.POST)
	public ResponseForm addActivity(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.addActivity(param,request);
	}

	/**
	 * 修改活动
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editActivity", method = RequestMethod.POST)
	public ResponseForm editActivity(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.editActivity(param,request);
	}

	/**
	 * 获取活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getActivityGoodsList", method = RequestMethod.POST)
	public ResponseForm getActivityGoodsList(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getActivityGoodsList(param,request);
	}
	
	/**
	 * 搜索活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchActivityGoodsList", method = RequestMethod.POST)
	public ResponseForm searchActivityGoodsList(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.searchActivityGoodsList(param,request);
	}
	
	
	/**
	 * 首页活动列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/activityIndex", method = RequestMethod.POST)
	public ResponseForm activityIndex(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.activityIndex(param,request);
	}

	
	/**
	 * 获取活动商品列表(带优惠信息)(不用了)
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getActivityGoodsListWithCoupon", method = RequestMethod.POST)
	public ResponseForm getActivityGoodsListWithCoupon(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getActivityGoodsListWithCoupon(param,request);
	}
	

	/**
	 * 查询活动优惠券
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getActivityCoupon", method = RequestMethod.POST)
	public ResponseForm getActivityCoupon(@RequestBody RequestForm param,HttpServletRequest request) {
		return storeActivityService.getActivityCoupon(param,request);
	}

}
