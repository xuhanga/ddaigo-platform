package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.GoodsService;
import com.purchase.service.UserService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;



@Controller
@RequestMapping("/goods")
public class GoodsController {
	
	@Resource
	GoodsService goodsService;
	

	/**
	 * @Description  门店折扣条件列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/rateCondition", method = RequestMethod.POST)
	public ResponseForm rateCondition(@RequestBody RequestForm param) {
		return goodsService.rateCondition(param);
	}
	
	/**
	 * @Description  门店尺码条件列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/sizeCondition", method = RequestMethod.POST)
	public ResponseForm sizeCondition(@RequestBody RequestForm param) {
		return goodsService.sizeCondition(param);
	}
	
	/**
	 * @Description  门店商品列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getStockGoodsList", method = RequestMethod.POST)
	public ResponseForm getStockGoodsList(@RequestBody RequestForm param) {
		return goodsService.getStockGoodsList(param);
	}
	
	
	
	/**
	 * @Description  根据ID查询商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsById", method = RequestMethod.POST)
	public ResponseForm getGoodsById(@RequestBody RequestForm param) {
		return goodsService.getGoodsById(param);
	}
	
	/**
	 * @Description  根据商品同款标识查询同款商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsByRelation", method = RequestMethod.POST)
	public ResponseForm getGoodsByRelation(@RequestBody RequestForm param) {
		return goodsService.getGoodsByRelation(param);
	}
	
	/**
	 * @Description  添加商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addGoods", method = RequestMethod.POST)
	public ResponseForm update(@RequestBody RequestForm param) {
		return goodsService.addGoods(param);
	}
	
	/**
	 * @Description  添加商品(不同款,不同价)
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addMultiGoods", method = RequestMethod.POST)
	public ResponseForm addMultiGoods(@RequestBody RequestForm param) {
		return goodsService.addMultiGoods(param);
	}
	
	/**
	 * @Description  修改商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateGoods", method = RequestMethod.POST)
	public ResponseForm updateGoods(@RequestBody RequestForm param) {
		return goodsService.updateGoods(param);
	}
	
	/**
	 * @Description  同款商品统一更新
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateRelationGoods", method = RequestMethod.POST)
	public ResponseForm updateRelationGoods(@RequestBody RequestForm param) {
		return goodsService.updateRelationGoods(param);
	}
	
	
	/**
	 * @Description  门店搜索商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searcheGoods", method = RequestMethod.POST)
	public ResponseForm searcheGoods(@RequestBody RequestForm param) {
		return goodsService.searcheGoods(param);
	}
	
	/**
	 * 根据时间查询爆款商品
	 * 只显示两款
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getHotGoods",method = RequestMethod.POST)
	public ResponseForm getHotGoods(@RequestBody RequestForm param) {
		return goodsService.getHotGoods(param);
	}
	
	/**
	 * @desc pc管理商品列表直接更新商品信息(价格 上下架状态,同款商品一起更新)
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editGoodsByPc",method = RequestMethod.POST)
	public ResponseForm editGoodsByPc(@RequestBody RequestForm param) {
		return goodsService.editGoodsByPc(param);
	}

}
