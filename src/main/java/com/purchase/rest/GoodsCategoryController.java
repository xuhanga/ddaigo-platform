package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.GoodsCategoryService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/goodsCategory")
public class GoodsCategoryController {
	
	@Resource
	GoodsCategoryService goodsCategoryService;
	
	
	/**
	 * 获取分类信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsCategory", method = RequestMethod.POST)
	public ResponseForm getAllCityByPid(@RequestBody RequestForm param) {
		return goodsCategoryService.getGoodsCategoryByPid(param);
	}
	
	/**
	 * 获取分类信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsCategoryCount", method = RequestMethod.POST)
	public ResponseForm getGoodsCategoryCount(@RequestBody RequestForm param) {
		return goodsCategoryService.getGoodsCategoryCount(param);
	}

}
