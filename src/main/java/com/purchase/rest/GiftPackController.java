package com.purchase.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.GiftPackService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: GiftPackController.java
 * @Description: APP端礼包相关接口
 * @author: liuhoujie
 * @date: 2018年6月27日
 */
@Controller
@RequestMapping("/gift")
public class GiftPackController {
	
	@Autowired
	GiftPackService giftPackService;
	
	/**
	 * @desc 获取礼包列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "getGiftPacks",method = RequestMethod.POST)
	public ResponseForm getGiftPacks() {
		return giftPackService.getGiftPacks();
	}
	
	/**
	 * @desc 完善用户信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "completedUserInfo",method = RequestMethod.POST)
	public ResponseForm completedUserInfo(@RequestBody RequestForm param) {
		return giftPackService.completedUserInfo(param);
	}
	
	
	
}
