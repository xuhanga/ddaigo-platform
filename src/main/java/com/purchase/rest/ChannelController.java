package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.ChannelService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/channel")
public class ChannelController {
	
	@Resource
	ChannelService channelService;
	
	/**
	 * @Title: channelList   
	 * @Description: 渠道列表  
	 * @return: ResponseForm
	 */
	@ResponseBody
	@RequestMapping(value="channelList",method=RequestMethod.POST)
	public ResponseForm channelList(@RequestBody RequestForm requestForm) {
		return channelService.channelList(requestForm);
	}
	
	/**
	 * @Title: addChannel   
	 * @Description: 新增渠道  
	 * @return: ResponseForm
	 */
	@ResponseBody
	@RequestMapping(value="addChannel",method=RequestMethod.POST)
	public ResponseForm addChannel(@RequestBody RequestForm requestForm) {
		return channelService.addChannel(requestForm);
	}
	
	/**
	 * @Title: editChannel   
	 * @Description: 编辑渠道  
	 * @return: ResponseForm
	 */
	@ResponseBody
	@RequestMapping(value="editChannel",method=RequestMethod.POST)
	public ResponseForm editChannel(@RequestBody RequestForm requestForm) {
		return channelService.editChannel(requestForm);
	}
	/**
	 * @Title: getChannel   
	 * @Description: 获取一个渠道  
	 * @return: ResponseForm
	 */
	@ResponseBody
	@RequestMapping(value="getChannel",method=RequestMethod.POST)
	public ResponseForm getChannel(@RequestBody RequestForm requestForm) {
		return channelService.getChannel(requestForm);
	}
	
	/**
	 * @desc 检查用户是否是推广者
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "checkChannelpeople",method = RequestMethod.POST)
	public ResponseForm checkChannelpeople(@RequestBody RequestForm param) {
		return channelService.checkChannelpeople(param);
	}  
}
