package com.purchase.rest;

import com.purchase.service.CouponService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/coupon")
public class CouponController {

    @Autowired
    private CouponService couponService;

    /**
     * 获取后台列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/listForManage", method = RequestMethod.POST)
    public ResponseForm listForManage(@RequestBody RequestForm param) {
        return couponService.listForManage(param);
    }

    /**
     * 添加信息
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseForm create(@RequestBody RequestForm param) {
        return couponService.create(param);
    }

    /**
     * 修改信息
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public ResponseForm modify(@RequestBody RequestForm param) {
        return couponService.modify(param);
    }

    /**
     * 获取领取列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/receiveList", method = RequestMethod.POST)
    public ResponseForm receiveList(@RequestBody RequestForm param) {
        return couponService.receiveList(param);
    }

    /**
     * 获取显示列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/myCouponList", method = RequestMethod.POST)
    public ResponseForm myCouponList(@RequestBody RequestForm param) {
        return couponService.myCouponList(param);
    }

    /**
     * 获取单个优惠券(通过id查询)
     * 查询推广人信息(推广码/手机号)
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getCouponById", method = RequestMethod.POST)
    public ResponseForm getCouponById(@RequestBody RequestForm param) {
        return couponService.getCouponById(param);
    }

    /**
     * 领取
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    public ResponseForm receive(@RequestBody RequestForm param) {
        return couponService.receive(param);
    }

    /**
     * 订单详情页可用优惠券列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderCouponList", method = RequestMethod.POST)
    public ResponseForm orderCouponList(@RequestBody RequestForm param) {
        return couponService.orderCouponList(param);
    }

    /**
     * 订单详情页使用优惠券
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderCouponUse", method = RequestMethod.POST)
    public ResponseForm orderCouponUse(@RequestBody RequestForm param) {
        return couponService.orderCouponUse(param);
    }

}
