package com.purchase.util;

import javax.annotation.Resource;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

/**
 * @ClassName: RedisUtil.java
 * @Description: redis数据库数据存储操作
 * @author: liuhoujie
 * @date: 2018年4月27日
 */
public class RedisUtil implements RedisTemplate {

	@Resource
	JedisPool jedisPool;

	@Override
	public void set(String key, String value) {
		 Jedis jedis = jedisPool.getResource();
		try {
			jedis.set(key, value);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}

	}

	@Override
	public void set(String key, String value, Integer seconds) {
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.set(key, value);
			jedis.expire(key, seconds);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	@Override
	public String get(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			String value = jedis.get(key);
			return value;
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		
	}

	@Override
	public void del(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.del(key);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	@Override
	public void expire(String key, Integer seconds) {
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.expireAt(key, seconds);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	@Override
	public long incr(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			Long value = jedis.incr(key);
			return value;
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

}
