package com.purchase.util;

import java.util.Map;

public class Page {

	/**
	 * @desc 计算起始条数和size 手动分页使用 
	 * @param param
	 * @return
	 */
	public static Map<String, Object> parse(Map<String, Object> param) {
			int page =1;
			if (param.containsKey("page") && null!=param.get("page"))
				page = Integer.parseInt(param.get("page").toString()); 
			int start = 0;
			int size = 6;
			if (param.containsKey("size") && null!=param.get("size"))
				size = Integer.parseInt(param.get("size").toString()); 
			if(page<1)
				page = 1;//默认第1页
			start = (page-1)*size;
			param.put("start", start);
			param.put("size", size);
		return param;
	}
	
	/**
	 * @DESC 计算起始页和size pagehelper使用
	 * @param param
	 * @return
	 */
	public static Map<String ,Object> parsePage(Map<String, Object> param){
		int page =1;
		if (param.containsKey("page") && null!=param.get("page"))
			page = Integer.parseInt(param.get("page").toString()); 
		int size = 6;
		if (param.containsKey("size") && null!=param.get("size"))
			size = Integer.parseInt(param.get("size").toString());
		param.put("page", page);
		param.put("size", size);
		return param;
	}
}
