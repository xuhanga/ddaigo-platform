package com.purchase.mapper;

import com.purchase.entity.Master;

import tk.mybatis.mapper.common.Mapper;

public interface MasterMapper extends Mapper<Master> {

}