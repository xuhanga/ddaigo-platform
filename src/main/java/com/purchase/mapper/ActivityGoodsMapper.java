package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.ActivityGoods;

import tk.mybatis.mapper.common.Mapper;

public interface ActivityGoodsMapper extends Mapper<ActivityGoods> {

	
	List<Map<String, Object>> getActivityByType(Map<String, Object> paramMap);

	void updateByRelation(Map paramMap);
}
