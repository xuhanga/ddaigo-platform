package com.purchase.mapper;

import com.purchase.entity.Coupon;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@MyBatisRepository
public interface CouponMapper extends Mapper<Coupon> {


    /**
     * 获取优惠券list
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getCouponList(Map<String, Object> paramMap);

    /**
     * 添加购物券
     *
     * @param coupon
     * @return
     */
    int createCoupon(Coupon coupon);

    /**
     * 修改购物券
     *
     * @param paramMap
     * @return
     */
    int updateCoupon(Map<String, Object> paramMap);

    /**
     * 关联优惠券-活动
     *
     * @param list
     * @return
     */
    int createCouponActivity(List<Map<String, Object>> list);

    /**
     * 发放列表
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getReleaseList(Map<String, Object> paramMap);

    /**
     * 领取列表
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getReceiveList(Map<String, Object> paramMap);

    /**
     * 我的优惠券（未使用）
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getMyCouponListUnused(Map<String, Object> paramMap);

    /**
     * 我的优惠券（已过期）
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getMyCouponListExpired(Map<String, Object> paramMap);

    /**
     * 我的优惠券（已使用）
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getMyCouponListUsed(Map<String, Object> paramMap);

    /**
     * 领取优惠券（关联优惠券-用户）
     *
     * @param list
     * @return
     */
    int createCouponUser(List<Map<String, Object>> list);

    /**
     * 订单详情页可用优惠券列表
     *
     * @param paramMap
     * @return
     */
    List<Coupon> getOrderCouponList(Map<String, Object> paramMap);

    /**
     * 获取优惠券
     *
     * @param orderId
     * @return
     */
    Coupon getCouponByOrderId(@Param("orderId") String orderId);

    /**
     * 订单第一次使用优惠券
     *
     * @param paramMap
     * @return
     */
    int useCoupon(Map<String, Object> paramMap);

    /**
     * 订单不使用优惠券
     *
     * @param paramMap
     * @return
     */
    int notUseCoupon(Map<String, Object> paramMap);

    /**
     * 获取单个优惠券(通过id查询)
     *
     * @param param
     * @return
     */
    Map getCouponById(Integer id);
	Map getCouponByIdWithCondition(Integer id);
    /**
     * 获取活动id(多个)
     *
     * @param param
     * @return
     */
    List getActivityInfoByCouponId(Integer id);



    /**
     * 修改优惠券支付状态
     *
     * @param orderId
     * @return
     */
    int payCoupon(@Param("orderId") String orderId);

    /**
     * 用户已领取的优惠券（不包含已用于支付的优惠券）
     * @param paramMap
     * @return
     */
    List<Integer> getReceivedIdListV1(Map<String, Object> paramMap);

    /**
     * 用户已领取的优惠券（包含已用于支付的优惠券）
     * @param paramMap
     * @return
     */
    List<Integer> getReceivedIdListV2(Map<String, Object> paramMap);


	List<Coupon> getMemberCoupons(int i);

    BigDecimal getOrderCouponPrice(@Param("orderId") String orderId);




    void delCouponActivity(Map<String, Object> paramMap);

    /**
     * 获取所有支持全部活动的优惠券
     * @return
     */
    List<Coupon> getAllActivityCoupons();

    /**
     * 获取新人专属优惠券
     * @return
     */
    List<Coupon> getNewUserCoupons();

    List<Map<String,Object>> getActivityInfoByCouponIdV2(@Param("id")Integer id);
}
