package com.purchase.mapper;

import com.purchase.entity.Director;

import tk.mybatis.mapper.common.Mapper;

public interface DirectorMapper extends Mapper<Director> {

}