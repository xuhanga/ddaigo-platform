/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: WxUser.java
 * @Package: com.ddyx.mapper
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年12月14日 下午5:50:09
 * 
 * *************************************
 */
package com.purchase.mapper;


import com.purchase.entity.UserSession;

import tk.mybatis.mapper.common.Mapper;

/**
 * @ClassName: UserSessionMapper.java
 * @Module: 
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2017年12月14日 下午5:50:09
 * 
 */
public interface UserSessionMapper extends Mapper<UserSession> {

	public UserSession getUserSessionBySid(String sessionId);
}
