package com.purchase.mapper;

import com.purchase.entity.GoodsImage;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface GoodsImageMapper extends Mapper<GoodsImage>  {

	String getGoodsImageUrl(Integer goodsId);
}
