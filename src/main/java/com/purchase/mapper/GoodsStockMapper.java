package com.purchase.mapper;

import com.purchase.entity.GoodsStock;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface GoodsStockMapper extends Mapper<GoodsStock>  {

	
	
}
