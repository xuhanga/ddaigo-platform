package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.Area;
import com.purchase.entity.Store;
import com.purchase.util.RequestForm;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface StoreMapper extends Mapper<Store>  {

	
	//查询地区
	List<Map<String, Object>> getBrandByStoreNo(String storeNo);
	
	
	
}
