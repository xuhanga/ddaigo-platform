package com.purchase.mapper;

import com.purchase.entity.GoodsCategory;

import tk.mybatis.mapper.common.Mapper;

public interface GoodsCategoryMapper extends Mapper<GoodsCategory>{

}
