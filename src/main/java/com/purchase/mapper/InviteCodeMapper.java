package com.purchase.mapper;

import com.purchase.entity.InviteCode;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@MyBatisRepository
public interface InviteCodeMapper extends Mapper<InviteCode> {

    int insertCodeBatch(List<InviteCode> inviteCodes);

    Integer getCountCode(@Param("code") String code);

    void disableInviteCode(@Param("wxUid") String wxUid, @Param("inviteCode") String inviteCode);

    List<InviteCode> getInviteCodeList(@Param("createWxUid") String createWxUid);
}