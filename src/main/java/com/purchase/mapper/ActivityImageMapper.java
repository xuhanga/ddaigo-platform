package com.purchase.mapper;

import com.purchase.entity.ActivityImage;

import tk.mybatis.mapper.common.Mapper;

public interface ActivityImageMapper extends Mapper<ActivityImage> {
}
