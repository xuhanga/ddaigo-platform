package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface GiftPackService {

	ResponseForm getGiftPacks();

	ResponseForm completedUserInfo(RequestForm param);

}
