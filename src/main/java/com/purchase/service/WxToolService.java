package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface WxToolService {
	/**
	 * @Title: getQRCode_wxProgram   
	 * @Description: 威信小程序的二维码
	 * @author: liuhoujie
	 * @param: @param param
	 * @param: @return      
	 * @return: ResponseForm
	 */
	public ResponseForm getQRCode_wxProgram(RequestForm param) ;
	
	
}
