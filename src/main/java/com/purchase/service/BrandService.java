package com.purchase.service;

import javax.servlet.http.HttpServletRequest;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface BrandService {

	
	/**
	 * 获取品牌信息
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm getBrandList(RequestForm param, HttpServletRequest request);
	
	/**
	 * 添加品牌信息
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm addBrand(RequestForm param, HttpServletRequest request);

	/**
	 * 修改品牌信息
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm editBrand(RequestForm param, HttpServletRequest request);

	/**
	 * 根据ID获取品牌信息
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm getBrandById(RequestForm param, HttpServletRequest request);
}
