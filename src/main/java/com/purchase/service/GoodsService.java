package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface GoodsService {
	
	ResponseForm rateCondition(RequestForm param);

	ResponseForm sizeCondition(RequestForm param);

	ResponseForm getStockGoodsList(RequestForm param);

	ResponseForm getGoodsById(RequestForm param);

	ResponseForm addGoods(RequestForm param);

	ResponseForm updateGoods(RequestForm param);

	ResponseForm searcheGoods(RequestForm param);

	ResponseForm getHotGoods(RequestForm param);

	ResponseForm editGoodsByPc(RequestForm param);

	ResponseForm addMultiGoods(RequestForm param);

	ResponseForm getGoodsByRelation(RequestForm param);

	ResponseForm updateRelationGoods(RequestForm param);
}
