package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import java.util.Map;

public interface CouponService {

    /**
     * 后台优惠券列表
     *
     * @param param
     * @return
     */
    ResponseForm listForManage(RequestForm param);

    /**
     * 后台添加优惠券
     *
     * @param param
     * @return
     */
    ResponseForm create(RequestForm param);

    /**
     * 后台修改优惠券
     *
     * @param param
     * @return
     */
    ResponseForm modify(RequestForm param);

    /**
     * 领取优惠券
     *
     * @param param
     * @return
     */
    ResponseForm receive(RequestForm param);

    /**
     * 优惠券领取列表
     *
     * @param param
     * @return
     */
    ResponseForm receiveList(RequestForm param);

    /**
     * 优惠券显示列表
     *
     * @param param
     * @return
     */
    ResponseForm myCouponList(RequestForm param);


    /**
     * 获取单个优惠拳(通过id查询)
     *
     * @param param
     * @return
     */
    ResponseForm getCouponById(RequestForm param);

    /**
     * 订单详情页可用优惠券列表
     *
     * @param param
     * @return
     */
    ResponseForm orderCouponList(RequestForm param);

    /**
     * 订单详情页使用优惠券
     *
     * @param param
     * @return
     */
    ResponseForm orderCouponUse(RequestForm param);

    /**
     * 给用户绑定新用户专属优惠券
     * @param wxUid
     */
    public void bindNewUserCoupons(String wxUid,String tradeType);
}
