package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import javax.servlet.http.HttpServletRequest;

public interface PayService {

    /**
     * 统一下单
     * @param param
     * @return
     */
    ResponseForm doUnifiedOrder(RequestForm param);
    
    /**
     * 399礼包下单
     * @param param
     * @return
     */
	ResponseForm doUnifiedOrderForGiftPack(RequestForm param);
	
	 /**
     * 88会员充值下单
     * @param param
     * @return
     */
	ResponseForm doUnifiedOrderForRechargeMember(RequestForm param);
    /**
     * 接收微信支付结果通知
     * @param param
     * @return
     */
    String payNotify(HttpServletRequest param);
    
    /**
     * 接收399礼包支付微信支付结果通知
     * @param param
     * @return
     */
    String payNotifyForGiftPack(HttpServletRequest request);
    
    /**
     * 接收88会员充值支付微信支付结果通知
     * @param param
     * @return
     */
    String payNotifyForRechargeMember(HttpServletRequest request);

    /**
     * 查询订单
     *
     * @param param
     * @return
     */
    ResponseForm orderQuery(RequestForm param);


}
