package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import java.util.ArrayList;

public interface InviteCodeService {

    ResponseForm createCode(RequestForm param);

    ResponseForm getCode(RequestForm param);

    /**
     * 获取邀请码
     *
     * @param wxUid       邀请码生成者
     * @param num         生成个数
     * @param createClass 邀请码级别   0：父级（管理员生成一级代理用），1：子级（生成二级及以下用）
     * @return
     */
    ArrayList<String> generateCode(String wxUid, String num, String createClass);

    /**
     * 使用邀请码
     *
     * @param wxUid      邀请码使用者
     * @param inviteCode 邀请码
     */
    void useCode(String wxUid, String inviteCode);

    /**
     * APP端 用户填写推广码后绑定推广码
     * @param param
     * @return
     */
	ResponseForm bindParentInviteCode(RequestForm param);
	
    /**
     * 小程序端 扫码进入小程序绑定邀请码
     * @param param
     * @return
     */
	ResponseForm bindInviteCodeByMiniProgram(RequestForm param);

}
