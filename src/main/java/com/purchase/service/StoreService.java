package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface StoreService {

	ResponseForm getStoreList(RequestForm param);

	ResponseForm getStoreByStroreNo(RequestForm param);

	ResponseForm getStoreById(RequestForm param);

	ResponseForm addStore(RequestForm param);

	ResponseForm editStore(RequestForm param);

	ResponseForm getBrand(RequestForm param);

	ResponseForm getStores(RequestForm param);

}
