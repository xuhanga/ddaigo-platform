package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface WithdrawRecordService {

    ResponseForm addWithdrawRecord(RequestForm requestForm);

    ResponseForm getWithdrawRecordList(RequestForm requestForm);

    ResponseForm getWithdrawRecordListForManage(RequestForm requestForm);

    ResponseForm approveWithdrawRecord(RequestForm requestForm);
}
