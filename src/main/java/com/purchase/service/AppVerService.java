package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface AppVerService {

	ResponseForm getVerInfo(RequestForm param);

	ResponseForm newVer(RequestForm param);

	ResponseForm isOnline();

	ResponseForm touristVisit();

	ResponseForm changeIosAppVer(RequestForm param);

}
