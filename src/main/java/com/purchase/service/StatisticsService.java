package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface StatisticsService {

	ResponseForm getSalesAndPriceData(RequestForm param);

	ResponseForm getActivityOrderData(RequestForm param);

	/**
	 * @Description: 推广信息统计
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return : 人数,钱,单量,提成
	 */
	ResponseForm channelInfoStatistics(RequestForm param);
	
	/**
	 * @Description: 推广用户信息明细
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return : 用户明细
	 */
	ResponseForm channelUserDetails(RequestForm param);
	
	/**
	 * @Description: 推广用户订单明细
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return : 用户明细
	 */
	ResponseForm channelUserOrders(RequestForm param);
}
