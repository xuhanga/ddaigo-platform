package com.purchase.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.entity.AppUser;
import com.purchase.entity.Goods;
import com.purchase.entity.GoodsImage;
import com.purchase.mapper.AppUserMapper;
import com.purchase.mapper.GoodsImageMapper;
import com.purchase.mapper.GoodsMapper;
import com.purchase.service.GiftPackService;
import com.purchase.util.RedisUtil;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@SuppressWarnings("unused")
public class GiftPackServiceImpl implements GiftPackService {

	@Autowired
	GoodsMapper goodsMapper;
	@Autowired
	GoodsImageMapper goodsImageMapper;
	@Autowired
	AppUserMapper appUserMapper;
	@Autowired
	RedisUtil redisUtil;

	@Override
	public ResponseForm getGiftPacks() {
		ResponseForm result = new ResponseForm();
		Goods goods = new Goods();
		goods.setGiftPack(1);
		goods.setSoldout(2);
		List<Goods> goodsList = goodsMapper.select(goods);
		for (Goods item : goodsList) {
			GoodsImage image = new GoodsImage();
			image.setGoodsId(item.getGoodsId());
			List<GoodsImage> images = goodsImageMapper.select(image);
			item.setGoodsImages(images);
		}

		result.succ("操作成功", goodsList);
		return result;
	}

	@Override
	public ResponseForm completedUserInfo(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String personName = (String) paramMap.get("personName");
			String weixinId = (String) paramMap.get("weixinId");
			String alipayId = (String) paramMap.get("alipayId");
			String verifyPhone = (String) paramMap.get("verifyPhone");
			String submitCode = (String) paramMap.get("verifyCode");
			String wxUid = (String) paramMap.get("wxUid");
			if (StringUtils.isBlank(personName) || StringUtils.isBlank(weixinId) || StringUtils.isBlank(verifyPhone)
					|| StringUtils.isBlank(submitCode) || StringUtils.isBlank(wxUid)) {
				result.fail("400", "参数错误");
				return result;
			}

			String verifyCode = redisUtil.get("verify_" + verifyPhone);
			if (verifyCode==null||!verifyCode.equals(submitCode)) {
				result.fail("300", "验证码超时或错误");
			}

			try {

				AppUser appUser = new AppUser();
				appUser.setWxUid(wxUid);
				appUser = appUserMapper.selectOne(appUser);
				if (appUser == null) {
					result.fail("400", "该用户不存在");
					return result;
				}

				AppUser appUser2 = new AppUser();
				appUser2.setVerifyPhone(verifyPhone);
				appUser2 = appUserMapper.selectOne(appUser2);
				if (appUser2 != null && !appUser2.getWxUid().equals(appUser.getWxUid())) {
					result.fail("400", "该手机号已被其他用户绑定");
					return result;
				}

				appUser.setPersonName(personName);
				appUser.setWeixinId(weixinId);
				if (StringUtils.isNotBlank(alipayId))
					appUser.setAlipayId(alipayId);
				appUser.setVerifyPhone(verifyPhone);
				appUserMapper.updateByPrimaryKeySelective(appUser);

				result.succ("200", "操作成功");
			} catch (Exception e) {
				e.printStackTrace();
				log.error("数据库访问错误");
				result.fail("400", "参数错误");
			}
		}
		return result;
	}

}
